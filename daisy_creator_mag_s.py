#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Autor: Joerg Sorge
Distributed under the terms of GNU GPL version 2 or later
Copyright (C) Joerg Sorge joergsorge at googel
2012-06-20

This program is for
- copy mp3 files for processing for DAISY Talking Books
- create DAISY Fileset
For now, it's only possible to create a flat DAISY-Structure

Dieses Programm
- kopiert mp3-Files fuer die Verarbeitung zu Daisy-Buechern
- erzeugt die noetigen Dateien fuer eine Daisy-Struktur.
Bisher kann nur eine DAISY-Ebene erzeugt werden.

Additional python modul necessary:
Zusatz-Module benoetigt:
python3-pyqt5
python3-mutagen
python3-html2text
lame
sudo apt-get install python3-pyqt5 python3-mutagen python3-html2text lame

Update gui with:
GUI aktualisieren mit:
pyuic5 daisy_creator_mag_s.ui -o daisy_creator_mag_s_ui.py
"""

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtGui import QMovie
import sys
import os
import shutil
import datetime
from datetime import timedelta
import subprocess
import re
from mutagen.mp3 import MP3
from mutagen.id3 import ID3
from mutagen.id3 import ID3NoHeaderError
import configparser
import daisy_creator_mag_s_ui


class TaskManager(QtCore.QObject):
    started = QtCore.pyqtSignal()
    finished = QtCore.pyqtSignal()
    progressChanged = QtCore.pyqtSignal(int, QtCore.QByteArray)

    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self._process = QtCore.QProcess(self)
        self._process.finished.connect(self.handleFinished)
        self._progress = 0

    def start_tasks(self, tasks):
        self._tasks = iter(tasks)
        self.fetchNext()
        self.started.emit()
        self._progress = 0

    def fetchNext(self):
        try:
            task = next(self._tasks)
        except StopIteration:
            return False
        else:
            self._process.start(*task)
        return True

    def processCurrentTask(self):
        output = self._process.readAllStandardOutput()
        self._progress += 1
        self.progressChanged.emit(self._progress, output)

    def handleFinished(self):
        self.processCurrentTask()
        if not self.fetchNext():
            self.finished.emit()


class DaisyCopy(QtWidgets.QMainWindow, daisy_creator_mag_s_ui.Ui_DaisyMain):
    """
    mainClass
    The second parent must be 'Ui_<obj. name of main widget class>'.
    """

    process_validator_error_signal = QtCore.pyqtSignal(str)
    process_validator_output_signal = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        """Settings"""
        super(DaisyCopy, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        self.app_debug_mod = "yes"
        self.app_mag_items = ["Zeitschrift"]
        self.app_issue_items_prev = ["10", "11", "12", "22", "23", "24", "25"]
        self.app_issue_items_current = [
            "01", "02", "03", "04", "05", "06", "07",
            "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
            "19", "20", "21", "22", "23", "24", "25"]
        self.app_issue_items_next = ["01", "02", "03", "04"]
        self.app_mag_path = QtCore.QDir.homePath()
        self.app_mag_path_dest = QtCore.QDir.homePath()
        self.app_mag_path_meta = QtCore.QDir.homePath()
        self.app_mag_path_issue_nr = QtCore.QDir.homePath()
        self.app_mag_path_intro = QtCore.QDir.homePath()
        self.app_mag_path_counter_file = QtCore.QDir.homePath()
        # we need ext package lame
        self.app_lame = ""
        # we need ext package daisy pipeline
        self.app_validator = ""
        self.app_validator_script = ""
        self.app_validator_time = ""
        self.app_validator_path = ""
        # for processes validator
        self.process_validator = QtCore.QProcess()
        self.process_validator.readyReadStandardError.connect(
            self.process_validator_on_ready_read_standard_error)
        self.process_validator.readyReadStandardOutput.connect(
            self.process_validator_on_ready_read_standard_output)
        # for find text in textEdit
        self.textDoc = QtGui.QTextDocument()
        # for mp3 files needed to recode
        self.dict_files_to_recode = {}
        # for process lame
        self.manager_bitrate = TaskManager(self)
        self.manager_bitrate.progressChanged.connect(
            self.process_bitrate_data_ready)
        # for animated gif
        self.movie = QMovie("cube-gif-6_cropped.gif")
        self.labelWaitBitrate.setMovie(self.movie)
        self.labelWaitValidate.setMovie(self.movie)
        self.connect_actions()

    def process_bitrate_data_ready(self, progress, result):
        """process bitrate data ready"""
        self.textEditConsole.append(str(result, "utf-8"))
        self.progressBarCopy.setValue(progress)

    def process_bitrate_finished(self):
        """process bitrate finished"""
        self.textEdit.append("<b>Bitrate aendern abgeschlossen</b><br>")
        self.commandLinkButton.setDisabled(False)
        self.commandLinkButtonDaisy.setDisabled(False)
        self.pushButtonValidate.setDisabled(False)
        self.commandLinkButton.setStyleSheet(
            "background-color: yellow")
        self.movie.stop()
        self.labelWaitBitrate.hide()
        self.check_change_id3_preparation()

    def process_validator_on_ready_read_standard_error(self):
        """process validator on ready read standard error"""
        process_error = (
            self.process_validator.readAllStandardError().data().decode())
        self.textEditConsole.append(process_error)
        self.process_validator_error_signal.emit(process_error)

    def process_validator_on_ready_read_standard_output(self):
        """process validator on ready read standard output"""
        result = self.process_validator.readAllStandardOutput().data().decode()
        self.textEditConsole.append(result)
        self.process_validator_output_signal.emit(result)

    def process_validator_finished(self):
        """process validator finished"""
        self.textEditValidate.append("Validierung abgeschlossen")
        # search for SUCCESS
        # the cursor in self.textEditValidate is on the end of text,
        # so we must search backwards
        flag = self.textDoc.FindBackward
        success = self.textEditConsole.find('SUCCESS', flag)
        self.show_debug_message(success)

        if not success:
            self.pushButtonValidate.setStyleSheet(
                "background-color: yellow")
            self.validate_daisy_load_report()
        else:
            log_message = "Daisy-Validierung erfolgreich!"
            self.show_debug_message(log_message)
            self.textEditValidate.append(
                "<b>Daisy-Validierung erfolgreich!</b>")
            self.pushButtonValidate.setStyleSheet(
                "background-color: green")

        self.commandLinkButton.setDisabled(False)
        self.commandLinkButtonDaisy.setDisabled(False)
        self.pushButtonValidate.setDisabled(False)
        self.movie.stop()
        self.labelWaitValidate.hide()

    def connect_actions(self):
        """Define Actions """
        self.toolButtonCopySource.clicked.connect(self.action_open_copy_source)
        self.toolButtonCopyDest.clicked.connect(self.action_open_copy_dest)
        self.toolButtonCopyFile1.clicked.connect(self.action_open_counter_file)
        self.toolButtonMetaFile.clicked.connect(self.action_open_meta_file)
        self.commandLinkButton.clicked.connect(self.action_run_copy)
        self.commandLinkButtonDaisy.clicked.connect(self.action_run_daisy)
        self.toolButtonDaisySource.clicked.connect(
            self.action_open_daisy_source)
        self.pushButtonValidate.clicked.connect(
            self.action_validate_daisy)
        self.pushButtonClose1.clicked.connect(self.action_quit)
        self.pushButtonClose2.clicked.connect(self.action_quit)
        self.pushButtonClose3.clicked.connect(self.action_quit)
        self.pushButtonClose4.clicked.connect(self.action_quit)

    def read_config(self):
        """read Config from file"""
        self.show_debug_message("read Config from file")
        file_exist = os.path.isfile("daisy_creator_mag_s.config")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEditConsole.append(
                "<font color='red'>"
                + "Config-Datei konnte nicht geladen werden: </font><br>"
                + "daisy_creator_mag.config<br>"
                + "Die Programmnutzung ohne diese Datei ist nicht möglich!")
            log_message = ("Config-Datei konnte nicht geladen werden!<br>"
                           + "Siehe Fehlermeldung")
            self.show_dialog_critical(log_message)
            self.action_tab_select(6)
            return file_exist

        config = configparser.RawConfigParser()
        config.read("daisy_creator_mag_s.config")
        try:
            self.app_mag_path = config.get('Folders', 'Source')
            self.app_mag_path_dest = config.get('Folders', 'Destination')
            self.app_mag_path_meta = config.get('Folders', 'Meta')
            self.app_mag_path_issue_nr = config.get(
                'Folders', 'Issue_nr')
            self.app_mag_path_intro = config.get('Folders', 'Intros')
            self.app_mag_path_counter_file = config.get(
                'Folders', 'Itemlist')
            self.app_mag_items = config.get(
                'Magazines', 'Magazines').split(",")
            self.app_lame = config.get('Programs', 'LAME')
            self.app_validator = config.get('Programs', 'DaisyPipeline')
            self.app_validator_script = config.get(
                'Programs', 'DaisyPipeline_Script')
            self.app_validator_time = config.get(
                'Programs', 'DaisyPipeline_Time')
            self.app_validator_path = config.get(
                'Programs', 'DaisyPipeline_Report')
        except (configparser.NoSectionError, configparser.NoOptionError):
            log_message = ("Fehler beim laden der Meta-Daten:<br>"
                           + "Sektion oder Option fehlt")
            self.show_debug_message(log_message)
            self.textEdit.append(
                "<font color='red'>" + log_message + "</font>")
            self.show_dialog_critical(log_message)

    def read_help(self):
        """read Readme from file"""
        file_exist = os.path.isfile("README.md")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Hilfe-Datei konnte nicht geladen werden: </font>"
                + "README.md")
            return

        fobj = open("README.md")
        for line in fobj:
            self.textEditHelp.append(line)
        # set cursor on top of helpfile
        cursor = self.textEditHelp.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start,
                            QtGui.QTextCursor.MoveAnchor, 0)
        self.textEditHelp.setTextCursor(cursor)
        fobj.close()

    def action_open_copy_source(self):
        """Source of copy"""
        # QtCore.QDir.homePath()
        dir_source = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Quell-Ordner",
            self.app_mag_path
            )
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditCopySource.setText(dir_source)
            self.textEdit.append("<b>Quelle:</b>")
            self.textEdit.append(dir_source)

    def action_open_daisy_source(self):
        """Source of daisy"""
        dir_source = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Quell-Ordner",
            self.app_mag_path_dest
            )
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditDaisySource.setText(dir_source)
            self.textEdit.append("Quelle:")
            self.textEdit.append(dir_source)

    def action_open_copy_dest(self):
        """Destination of Copy"""
        dir_dest = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Ziel-Ordner",
            self.app_mag_path_dest
            )
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_dest:
            self.lineEditCopyDest.setText(dir_dest)
            self.textEdit.append("<b>Ziel:</b>")
            self.textEdit.append(dir_dest)

    def action_open_counter_file(self):
        """counter file for renaming of files"""
        counter_file = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Counter-Datei",
            self.app_mag_path_counter_file
            )
        # Don't attempt to open if open dialog
        # was cancelled away.
        if counter_file:
            self.lineEditFileCounter.setText(counter_file[0])
            self.textEdit.append("<b>Counter-Datei:</b>")
            self.textEdit.append(counter_file[0])

    def action_open_meta_file(self):
        """Metafile to load"""
        meta_file = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Daisy_Meta",
            self.app_mag_path_meta
            )
        # Don't attempt to open if open dialog
        # was cancelled away.
        if meta_file:
            self.lineEditMetaSource.setText(meta_file[0])
            self.read_meta_file(str(meta_file[0]))

    def action_run_copy(self):
        """main function for copy"""
        if self.lineEditCopySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        if self.lineEditCopyDest.text() == "Ziel-Ordner":
            error_message = "Ziel-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        if self.comboBoxSplitterAuthorTitle.currentText() != "_-_":
            error_message = (
                "Bisher wird nur diese Option <br>"
                + "zur Trennung von Autor und Titel unterstützt:<br>"
                + "_-_ ")
            self.show_dialog_critical(error_message)
            self.comboBoxSplitterAuthorTitle.setStyleSheet(
                "QComboBox" "{" "background-color: yellow;" "}")
            self.comboBoxSplitterAuthorTitle.setCurrentIndex(0)
            self.tabWidget.setCurrentIndex(0)
            return

        if self.lineEditFileCounter.text() == "Counter-Datei waehlen":
            if self.comboBoxCopyBhz.currentText() == "ABC_Journal":
                error_message = "Counter-Datei wurde nicht ausgewaehlt.."
                self.show_dialog_critical(error_message)
                return
        else:
            counter_lines = self.read_counter_file()
            n_counter_lines = len(counter_lines)

        self.show_debug_message(self.lineEditCopySource.text())
        self.show_debug_message(self.lineEditCopyDest.text())

        # check for files in source
        try:
            dirs_source = os.listdir(self.lineEditCopySource.text())
        except Exception as e:
            log_message = "read_files_from_dir Error: %s" % str(e)
            self.show_debug_message(log_message)

        # ceck dir of dest
        if os.path.exists(self.lineEditCopyDest.text()) is False:
            error_message = "Ziel-Ordner existiert nicht.."
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            self.lineEditCopyDest.setFocus()
            return

        self.show_debug_message(dirs_source)
        # add number to counter if copy AusgAnsage and/or Intro
        n_counter = 0
        if self.checkBoxCopyBhzIntro.isChecked():
            n_counter += 1
        if self.checkBoxCopyBhzIssueAnnouncement.isChecked():
            n_counter += 1

        n_count_mp3_files = self.count_mp3_files(dirs_source)
        if n_counter_lines != n_count_mp3_files:
            self.textEdit.append(
                "<b><font color='red'>"
                + "Anzahl der mp3-Dateien (" + str(n_count_mp3_files)
                + ") stimmt nicht mit Counter-Datei ("
                + str(n_counter_lines)
                + ") ueberein!</font></b>")

        files_ok = self.compare_files(dirs_source, counter_lines)
        if files_ok is None:
            return

        self.textEdit.append("<b>Kopieren:</b>")
        self.show_debug_message("Kopieren...")
        self.show_debug_message(n_count_mp3_files)
        dirs_source.sort()
        # loop trough files
        n_count_itm_dir = 0
        for item in dirs_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue

            file_to_copy_source = self.lineEditCopySource.text() + "/" + item
            # check if file exist
            file_exist = os.path.isfile(file_to_copy_source)
            if file_exist is False:
                self.show_debug_message("File not exists")
                # change max number and update progress
                n_count_mp3_files = n_count_mp3_files - 1
                n_count_prog_bar = n_count_itm_dir * 100 / n_count_mp3_files
                self.progressBarCopy.setValue(n_count_prog_bar)
                self.textEdit.append(
                    "<b>Datei konnte nicht kopiert werden: </b>"
                    + file_to_copy_source)
                self.show_debug_message(file_to_copy_source)
                self.textEdit.append("<b>Uebersprungen</b>:")
                self.textEdit.append(file_to_copy_source)
                continue

            # search filename in counterfile,
            # extract counter and set it on top of the filename
            self.show_debug_message("search filename in counterfile")
            self.show_debug_message(item)
            n_found = 0
            for line in counter_lines:
                n_len_line = len(line) - 1
                if line[3:n_len_line] == item:
                    n_found += 1
                    self.show_debug_message("counterline: " + line)
                    self.show_debug_message("counter: " + line[0:2])
                    # Counter on top
                    n_file_count = n_counter + int(line[0:2])
                    file_to_copy_dest = (
                        self.lineEditCopyDest.text() + "/"
                        + str(n_file_count).zfill(2) + "_" + item)

            # filename not found
            if n_found == 0:
                self.textEdit.append(
                    "<b><font color='red'>"
                    + "Dateiname in Counterdatei nicht gefunden</font></b>:")
                self.textEdit.append(
                    os.path.basename(str(file_to_copy_source)))
                self.textEdit.append("<b>Bearbeitung abgebrochen</b>:")
                return

            self.textEdit.append(os.path.basename(str(file_to_copy_dest)))
            self.show_debug_message(file_to_copy_source)
            self.show_debug_message(file_to_copy_dest)

            # check bitrate
            bitrate_ok = self.check_bitrate(
                file_to_copy_source, file_to_copy_dest)

            # nothing to do, only copy
            if bitrate_ok:
                self.copy_file(file_to_copy_source, file_to_copy_dest)
            else:
                self.textEdit.append("<b>Kopieren...</b>")

            n_count_itm_dir += 1
            self.show_debug_message(n_count_itm_dir)
            self.show_debug_message(n_count_mp3_files)
            n_count_prog_bar = n_count_itm_dir * 100 / n_count_mp3_files
            self.show_debug_message(n_count_prog_bar)
            self.progressBarCopy.setValue(int(n_count_prog_bar))

        # recode
        self.show_debug_message(self.dict_files_to_recode)
        self.change_bitrate_and_copy()

        self.show_debug_message(n_count_itm_dir)

        if self.checkBoxCopyBhzIntro.isChecked():
            self.copy_intro()

        if self.checkBoxCopyBhzIssueAnnouncement.isChecked():
            self.copy_issue_announcement()

        # load metadata
        self.lineEditMetaSource.setText(
            self.app_mag_path_meta + "/Daisy_Meta_"
            + self.comboBoxCopyBhz.currentText())
        self.read_meta_file(str(self.lineEditMetaSource.text()))
        # enter path of source and destination
        self.lineEditDaisySource.setText(self.lineEditCopyDest.text())

    def check_packages(self, package):
        """
        check if package is installed
        needs subprocess, os
        http://stackoverflow.com/
        questions/11210104/check-if-a-program-exists-from-a-python-script
        """
        try:
            devnull = open(os.devnull, "w")
            subprocess.Popen(
                [package], stdout=devnull,
                stderr=devnull).communicate()
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                error_message = (
                    "Es fehlt das Paket:\n " + package
                    + "\nZur Nutzung des vollen Funktionsumfanges "
                    + "muss es installiert werden!")
                self.show_dialog_critical(error_message)
                self.textEdit.append(
                    "<font color='red'>Es fehlt das Paket: </font> " + package)

    def check_filename(self, file_name):
        """check for spaces and non ascii characters"""

        non_ascii = None
        self.textEdit.append(file_name)

        for c in file_name:
            if ord(c) > 128:
                non_ascii = True
                break

        if non_ascii:
            self.textEdit.append(
                "<font color='blue'>"
                + "Unerlaubtes Zeichen bei:</font><br>"
                + os.path.basename(str(file_name)))
            return None

        if file_name.find(" ") != -1:
            error_message = ("<b>Unerlaubtes Leerzeichen im Dateinamen!</b>")
            self.textEdit.append(error_message)
            self.tabWidget.setCurrentIndex(1)
            return None
        return "OK"

    def check_filenames(self, files_source):
        for item in files_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue
            check_ok = self.check_filename(item)
            if check_ok is None:
                return None
        return "OK"

    def copy_file(self, file_to_copy_source, file_to_copy_dest):
        """copy file"""
        try:
            shutil.copy(file_to_copy_source, file_to_copy_dest)
        except Exception as e:
            log_message = "copy_file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEdit.append(log_message + file_to_copy_dest)

    def copy_intro(self):
        """copy intro"""
        file_to_copy_source = (
            self.app_mag_path_intro + "/Intro_"
            + self.comboBoxCopyBhz.currentText() + ".mp3")
        file_to_copy_dest = (
            self.lineEditCopyDest.text() + "/02_"
            + self.comboBoxCopyBhz.currentText() + "_-_"
            + self.comboBoxCopyBhzAusg.currentText() + "_Intro.mp3")
        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Intro nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        self.copy_file(file_to_copy_source, file_to_copy_dest)
        # self.check_change_id3(file_to_copy_dest)

    def copy_issue_announcement(self):
        """copy announcement of issue"""
        self.show_debug_message("copy announcement of issue...")
        path_announcement_issue = (
            self.app_mag_path_issue_nr + "_"
            + self.comboBoxCopyBhz.currentText())
        self.show_debug_message("path source:")
        self.show_debug_message(path_announcement_issue)
        file_to_copy_source = (
            path_announcement_issue + "/"
            + self.comboBoxCopyBhz.currentText() + "_-_Ausgabe_"
            + self.comboBoxCopyBhzAusg.currentText() + ".mp3")
        file_to_copy_dest = (
            self.lineEditCopyDest.text() + "/01_"
            + self.comboBoxCopyBhz.currentText() + "_-_Ausgabe_"
            + self.comboBoxCopyBhzAusg.currentText() + ".mp3")
        self.show_debug_message("path/file source:")
        self.show_debug_message(file_to_copy_source)
        self.show_debug_message("path/file dest:")
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Ausgabeansage nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        self.copy_file(file_to_copy_source, file_to_copy_dest)
        # self.check_change_id3(file_to_copy_dest)

    def count_mp3_files(self, files_source):
        """count mp3 files"""
        n_counter = 0
        for item in files_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue
            else:
                n_counter += 1
        return n_counter

    def read_counter_file(self):
        """reaf counter file with filenames and counter """
        try:
            f = open(str(self.lineEditFileCounter.text()), 'r')
            lines = f.readlines()
            f.close()
            self.textEdit.append(
                "<b>Counter-Datei gelesen: </b>"
                + self.lineEditFileCounter.text())
        except IOError as err:
            (errno, strerror) = err.args
            lines = None
            self.show_debug_message(
                "read_from_file_lines_in_list: nicht lesbar: "
                + self.lineEditFileCounter.text())
        return lines

    def check_change_id3_preparation(self):
        """check id3 Tags, maybe kill it, preparation"""
        # check for files in dest
        self.textEdit.append(
            "<b>ID3-Tags pruefen...</b>")

        try:
            dir_dest = os.listdir(self.lineEditCopyDest.text())
        except OSError as e:
            error_message = "Quelle: %s" % str(e)
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            return

        is_changed = False
        dir_dest.sort()
        self.show_debug_message(dir_dest)
        for item in dir_dest:
            file_in_dest = self.lineEditCopyDest.text() + "/" + item
            self.textEdit.append(item)
            self.show_debug_message(file_in_dest)
            # check if file exist
            file_exist = os.path.isfile(file_in_dest)
            if file_exist is False:
                self.show_debug_message("File not exists")
                self.textEdit.append(
                    "<b>Datei konnte nicht auf ID3 geprueft werden: </b><br>"
                    + file_in_dest)
                self.show_debug_message(file_in_dest)
                self.textEdit.append("<b>Uebersprungen</b>:<br>")
                self.textEdit.append(file_in_dest)
                continue
            is_changed = self.check_change_id3(file_in_dest)

            if is_changed:
                self.textEdit.append("<b>ID3-Tags pruefen: </b>")

        self.textEdit.append("<b>ID3-Tag-Pruefung abgeschlossen </b>")

    def check_change_id3(self, file_to_copy_dest):
        """check id3 tags, remove it when present"""
        tag = None
        try:
            audio = ID3(str(file_to_copy_dest))
            tag = "yes"
        except ID3NoHeaderError:
            self.show_debug_message("No ID3 header found; skipping.")

        if tag is not None:
            if self.checkBoxCopyID3Change.isChecked():
                audio.delete()
                self.textEdit.append(
                    "<b>ID3 entfernt bei</b>: "
                    + os.path.basename(str(file_to_copy_dest)))
                self.show_debug_message(
                    "ID3 entfernt bei " + file_to_copy_dest)
            else:
                self.textEdit.append(
                    "<b>ID3 vorhanden, aber NICHT entfernt bei</b>: "
                    + file_to_copy_dest)

    def check_bitrate(self, file_to_copy_source, file_to_copy_dest):
        """check bitrate,
        when necessary add to dict for later recode in new destination"""

        audio_source = MP3(str(file_to_copy_source))
        if (audio_source.info.bitrate / 1000
                == int(self.comboBoxPrefBitrate.currentText())):
            if self.checkBoxCopyBitrateChange.isChecked():
                return True
            else:
                self.textEdit.append(
                    "<b>Bitrate wurde NICHT geaendern bei</b><br>: "
                    + file_to_copy_dest)
                return True

        # we need recoding, add to dict
        self.dict_files_to_recode[file_to_copy_source] = file_to_copy_dest
        self.textEdit.append(
            "<b>Vom Kopieren zurückgestellt für Aenderung der Bitrate</b>:<br>"
            + os.path.basename(str(file_to_copy_source)))
        return None

    def change_bitrate_and_copy(self):
        """recode in new destination"""

        if not self.dict_files_to_recode:
            self.textEdit.append("<b>Bitrate aendern nicht noetig</b>")
            return

        self.textEdit.append("<b>Bitrate aendern...</b>")
        tasks = []
        for s, d in self.dict_files_to_recode.items():
            self.textEdit.append(os.path.basename(str(s)))
            tasks.append(
                ("lame",
                 ["-b", self.comboBoxPrefBitrate.currentText(),
                  "-m", "m", s, d]))

        self.show_debug_message(tasks)
        self.progressBarCopy.setMaximum(len(tasks))
        self.textEditConsole.clear()
        self.commandLinkButton.setStyleSheet("background-color: red")
        self.commandLinkButton.setEnabled(False)
        self.commandLinkButtonDaisy.setEnabled(False)
        self.pushButtonValidate.setEnabled(False)
        self.movie.start()
        self.manager_bitrate.finished.connect(self.process_bitrate_finished)
        self.manager_bitrate.start_tasks(tasks)

    def compare_files(self, dirs_source, counter_lines):
        """compare files from source with conter file"""
        self.show_debug_message("compare_files: counter file list")
        self.show_debug_message(counter_lines)
        self.textEdit.append("<b>Dateinamen pruefen...</b>")
        check_ok = self.check_filenames(dirs_source)
        if check_ok is None:
            return

        self.textEdit.append("<b>Dateien pruefen...</b>")
        # loop trough files
        for item in dirs_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue

            n_found = 0
            for line in counter_lines:
                # remove line break
                n_len_line = len(line) - 1
                if line[3:n_len_line] == item:
                    n_found += 1

            if n_found == 0:
                self.textEdit.append(
                    "<b><font color='red'>"
                    + "Fuer diese mp3-Datei keine Entsprechung in "
                    + "Counterdatei gefunden"
                    + "</font></b>:")
                self.textEdit.append(item)
                self.textEdit.append("<b>Bearbeitung abgebrochen!</b>")
                return None

        # now let's take a look from the other site
        for line in counter_lines:
            # self.textEdit.append("counter: " + line[3:n_len_line])
            # remove line break
            n_len_line = len(line) - 1
            n_found = 0
            for item in dirs_source:
                if (item[len(item) - 4:len(item)] != ".MP3"
                        and item[len(item) - 4:len(item)] != ".mp3"):
                    continue
                if line[3:n_len_line] == item:
                    n_found += 1

            if n_found == 0:
                self.textEdit.append(
                    "<b><font color='red'>"
                    + "Keine mp3-Datei zu diesem Eintrag in "
                    + "Counterdatei gefunden</font></b>:")
                self.textEdit.append(line[3:n_len_line])
                self.textEdit.append("<b>Bearbeitung abgebrochen!</b>")
                return None
        return "OK"

    def encode_file(self, file_to_copy_source, file_to_copy_dest):
        """encode mp3-files"""
        self.show_debug_message("encode_file")
        # damit die uebergabe der befehle richtig klappt,
        # muessen alle cmds im richtigen zeichensatz als strings encoded sein
        c_lame_encoder = "/usr/bin/lame"
        self.show_debug_message("type c_lame_encoder")
        self.show_debug_message(type(c_lame_encoder))
        self.show_debug_message("file_to_copy_source")
        self.show_debug_message(type(file_to_copy_source))
        self.show_debug_message(file_to_copy_dest)
        self.show_debug_message("type(file_to_copy_dest)")
        self.show_debug_message(type(file_to_copy_dest))

        p = subprocess.Popen(
            [c_lame_encoder, "-b",
                self.comboBoxPrefBitrate.currentText(),
                "-m", "m", file_to_copy_source, file_to_copy_dest],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

        self.show_debug_message("returncode 0")
        self.show_debug_message(p[0])
        self.show_debug_message("returncode 1")
        self.show_debug_message(p[1])

        # erfolgsmeldung suchen, wenn nicht gefunden: -1
        p1 = p[1].decode()
        n_encode_percent = p1.find("(100%)")
        n_encode_percent_1 = p1.find("(99%)")
        self.show_debug_message(n_encode_percent)
        c_complete = "no"

        # bei kurzen files kommt die 100% meldung nicht,
        # deshalb auch 99% durchgehen lassen
        if n_encode_percent == -1:
            # 100% nicht erreicht
            if n_encode_percent_1 != -1:
                # aber 99
                c_complete = "yes"
        else:
            c_complete = "yes"

        if c_complete == "yes":
            log_message = "recoded_file: " + file_to_copy_source
            self.show_debug_message(log_message)
            return file_to_copy_dest
        else:
            log_message = "recode_file Error: " + file_to_copy_source
            self.show_debug_message(log_message)
            return None

    def read_meta_file(self, meta_file):
        """load meta file"""

        if meta_file is None:
            meta_file = (str(self.lineEditMetaSource.text())
                         + "/" + self.app_mag_prefix_meta)

        file_exist = os.path.isfile(self.lineEditMetaSource.text())
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Meta-Datei konnte nicht geladen werden</font>: "
                + os.path.basename(str(self.lineEditMetaSource.text())))
            return

        config = configparser.RawConfigParser()
        config.read(meta_file)

        try:
            self.lineEditMetaProducer.setText(config.get(
                'Daisy_Meta', 'Produzent'))
            self.lineEditMetaAutor.setText(config.get(
                'Daisy_Meta', 'Autor'))
            self.lineEditMetaTitle.setText(config.get(
                'Daisy_Meta', 'Titel'))
            self.lineEditMetaEdition.setText(config.get(
                'Daisy_Meta', 'Edition'))
            self.lineEditMetaNarrator.setText(config.get(
                'Daisy_Meta', 'Sprecher'))
            self.lineEditMetaKeywords.setText(
                config.get('Daisy_Meta', 'Stichworte'))
            self.lineEditMetaRefOrig.setText(config.get(
                'Daisy_Meta', 'ISBN/Ref-Nr.Original'))
            self.lineEditMetaPublisher.setText(config.get(
                'Daisy_Meta', 'Verlag'))
            self.lineEditMetaYear.setText(config.get(
                'Daisy_Meta', 'Jahr'))
        except (configparser.NoSectionError, configparser.NoOptionError):
            log_message = ("Fehler beim laden der Meta-Daten:"
                           + "Sektion oder Option fehlt")
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")
            self.show_dialog_critical(log_message)

    def action_run_daisy(self):
        """create Daisy-Fileset"""
        if self.lineEditDaisySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        # Audios einlesen
        try:
            dir_items = os.listdir(self.lineEditDaisySource.text())
        except Exception as e:
            log_message = "read_files_from_dir Error: %s" % str(e)
            self.show_debug_message(log_message)

        self.progressBarDaisy.setValue(10)
        self.show_debug_message(dir_items)
        self.textEditDaisy.append("<b>Folgende Audios werden bearbeitet:</b>")
        m_mp3 = 0
        n_list = len(dir_items)
        self.show_debug_message(n_list)
        dir_audios = []
        dir_items.sort()
        for item in dir_items:
            if (item[len(item) - 4:len(item)]
                    == ".MP3" or item[len(item) - 4:len(item)] == ".mp3"):
                dir_audios.append(item)
                self.textEditDaisy.append(item)
                m_mp3 += 1

        l_times = self.calc_audio_lengt(dir_audios)
        total_audio_length = l_times[0]
        l_total_elapsed_time = l_times[1]
        l_file_time = l_times[2]
        print(total_audio_length)
        total_time = timedelta(seconds=total_audio_length)
        # umwandlung von timedelta in string:
        # minuten und sekunden musten immer zweistllig sein,
        # damit einstellige stunde eine null bekommt :zfill(8)
        l_total_time = str(total_time).split(".")
        c_total_time = l_total_time[0].zfill(8)

        self.textEditDaisy.append("Gesamtlaenge: " + c_total_time)
        self.write_ncc(c_total_time, m_mp3, dir_audios)
        self.progressBarDaisy.setValue(20)
        self.write_master_smil(c_total_time, dir_audios)
        self.progressBarDaisy.setValue(50)
        self.write_smil(l_total_elapsed_time, l_file_time, dir_audios)
        self.progressBarDaisy.setValue(100)

    def calc_audio_lengt(self, dir_audios):
        """calc length of all audios"""
        total_audio_length = 0
        l_total_elapsed_time = []
        l_total_elapsed_time.append(0)
        l_file_time = []
        for item in dir_audios:
            file_to_check = os.path.join(
                str(self.lineEditDaisySource.text()), item)
            audio_source = MP3(file_to_check)
            self.show_debug_message(item + " " + str(audio_source.info.length))
            total_audio_length += audio_source.info.length
            l_total_elapsed_time.append(total_audio_length)
            l_file_time.append(audio_source.info.length)
            l_times = []
            l_times.append(total_audio_length)
            l_times.append(l_total_elapsed_time)
            l_times.append(l_file_time)
        return l_times

    def write_ncc(self, c_total_time, m_mp3, dir_audios):
        """write NCC-Page"""
        try:
            f_out_file = open(os.path.join(
                str(self.lineEditDaisySource.text()), "ncc.html"), 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return
        self.textEditDaisy.append("<b>NCC-Datei schreiben</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0'
            + ' Transitional//EN"'
            + ' "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
            + '\r\n')
        f_out_file.write(
            '<html xmlns="http://www.w3.org/1999/xhtml">' + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write(
            '<meta http-equiv="Content-type" '
            + 'content="text/html; charset=utf-8"/>' + '\r\n')
        f_out_file.write(
            '<title>' + self.comboBoxCopyBhz.currentText()
            + '</title>' + '\r\n')

        f_out_file.write(
            '<meta name="ncc:generator" '
            + 'content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:revision" content="1"/>' + '\r\n')

        today = datetime.date.today()
        f_out_file.write(
            '<meta name="ncc:producedDate" content="'
            + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:revisionDate" content="'
            + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:tocItems" content="'
            + str(m_mp3) + '"/>' + '\r\n')

        f_out_file.write(
            '<meta name="ncc:totalTime" content="'
            + c_total_time + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:narrator" content="'
            + self.lineEditMetaNarrator.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageNormal" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageFront" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageSpecial" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sidebars" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:prodNotes" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:footnotes" content="0"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:depth" content="'
            + str(self.spinBoxLevel.value()) + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:maxPageNormal" content="'
            + str(self.spinBoxPages.value()) + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:charset" content="utf-8"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:multimediaType" content="audioNcc"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:setInfo" content="1 of 1"/>' + '\r\n')

        f_out_file.write(
            '<meta name="ncc:sourceDate" content="'
            + self.lineEditMetaYear.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:sourceEdition" content="'
            + self.lineEditMetaEdition.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:sourcePublisher" content="'
            + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')

        # Anzahl files = Records 2x + ncc.html + master.smil
        f_out_file.write(
            '<meta name="ncc:files" content="'
            + str(m_mp3 + m_mp3 + 2) + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:producer" content="'
            + self.lineEditMetaProducer.text() + '"/>' + '\r\n')

        f_out_file.write(
            '<meta name="dc:creator" content="'
            + self.lineEditMetaAutor.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:date" content="'
            + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:identifier" content="'
            + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:language" content="de"'
            + ' scheme="ISO 639"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:publisher" content="'
            + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:source" content="'
            + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:subject" content="'
            + self.lineEditMetaKeywords.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:title" content="'
            + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        # Medibus-OK items
        f_out_file.write(
            '<meta name="prod:audioformat" content="wave 44 kHz"/>' + '\r\n')
        f_out_file.write(
            '<meta name="prod:compression" content="mp3 '
            + self.comboBoxPrefBitrate.currentText() + '/ kb/s"/>' + '\r\n')
        f_out_file.write('<meta name="prod:localID" content=" "/>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')
        n_counter = 0
        for item in dir_audios:
            n_counter += 1
            if n_counter == 1:
                f_out_file.write(
                    '<h1 class="title" id="cnt_0001">'
                    + '<a href="0001.smil#txt_0001">'
                    + self.lineEditMetaAutor.text() + ": "
                    + self.lineEditMetaTitle.text() + '</a></h1>' + '\r\n')
                continue
            # trennen
            item_split = self.split_filename(item)
            c_author = self.extract_author(item_split)
            c_title = self.extract_title(item_split)
            f_out_file.write(
                '<h' + str(self.spinBoxLevel.value())
                + ' id="cnt_' + str(n_counter).zfill(4) + '"><a href="'
                + str(n_counter).zfill(4) + '.smil#txt_'
                + str(n_counter).zfill(4) + '">'
                + c_author + " - " + c_title + '</a></h1>' + '\r\n')

        f_out_file.write("</body>" + '\r\n')
        f_out_file.write("</html>" + '\r\n')
        f_out_file.close

    def write_master_smil(self, c_total_time, dir_audios):
        """write MasterSmil-Page """
        try:
            f_out_file = open(
                os.path.join(
                    str(self.lineEditDaisySource.text()), "master.smil"), 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return
        self.textEditDaisy.append("<b>MasterSmil-Datei schreiben</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
            + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
        f_out_file.write('<smil>' + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:identifier" content="'
            + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:title" content="'
            + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:generator" '
            + 'content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:format" content="Daisy 2.0"/>' + '\r\n')
        f_out_file.write(
            '<meta name="ncc:timeInThisSmil" content="'
            + c_total_time + '" />' + '\r\n')

        f_out_file.write('<layout>' + '\r\n')
        f_out_file.write('<region id="txt-view" />' + '\r\n')
        f_out_file.write('</layout>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')

        n_counter = 0
        for item in dir_audios:
            n_counter += 1
            # trennen
            item_split = self.split_filename(item)
            c_author = self.extract_author(item_split)
            c_title = self.extract_title(item_split)
            f_out_file.write(
                '<ref src="' + str(n_counter).zfill(4) + '.smil" title="'
                + c_author + " - " + c_title + '" id="smil_'
                + str(n_counter).zfill(4) + '"/>' + '\r\n')

        f_out_file.write('</body>' + '\r\n')
        f_out_file.write('</smil>' + '\r\n')
        f_out_file.close

    def write_smil(self, l_total_elapsed_time, l_file_time, dir_audios):
        """write Smil-Pages"""
        n_counter = 0
        for item in dir_audios:
            n_counter += 1

            try:
                filename = str(n_counter).zfill(4) + '.smil'
                f_out_file = open(
                    os.path.join(
                        str(self.lineEditDaisySource.text()),
                        filename), 'w')
            except IOError as err:
                (errno, strerror) = err.args
                self.show_debug_message(
                            "I/O error({0}): {1}".format(errno, strerror))
                return
            self.textEditDaisy.append(
                str(n_counter).zfill(4) + ".smil - File schreiben")
            # split
            item_split = self.split_filename(item)
            c_title = self.extract_title(item_split)

            f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
            f_out_file.write(
                '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
                + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
            f_out_file.write('<smil>' + '\r\n')
            f_out_file.write('<head>' + '\r\n')
            f_out_file.write(
                '<meta name="ncc:generator" content="KOM-IN-DaisyCreator"/>'
                + '\r\n')
            total_elapsed_time = (
                timedelta(seconds=l_total_elapsed_time[n_counter - 1]))
            splitted_total_elapsed_time = str(total_elapsed_time).split(".")
            print(splitted_total_elapsed_time)
            total_elapsed_time_hhmmss = splitted_total_elapsed_time[0].zfill(8)
            if n_counter == 1:
                # first entry has only one split
                total_elapsed_time_milli_micro = "000"
            else:
                total_elapsed_time_milli_micro = (
                    splitted_total_elapsed_time[1][0:3])

            f_out_file.write(
                '<meta name="ncc:totalElapsedTime" content="'
                + total_elapsed_time_hhmmss + "."
                + total_elapsed_time_milli_micro + '"/>' + '\r\n')

            file_time = timedelta(seconds=l_file_time[n_counter - 1])
            splitted_file_time = str(file_time).split(".")
            file_time_hhmmss = splitted_file_time[0].zfill(8)
            # if no millissec, only one element is in the list
            if len(splitted_file_time) > 1:
                if len(splitted_file_time[1]) >= 3:
                    file_time_milli_micro = splitted_file_time[1][0:3]
                elif len(splitted_file_time[1]) == 2:
                    file_time_milli_micro = splitted_file_time[1][0:2]
            else:
                file_time_milli_micro = "000"

            f_out_file.write(
                '<meta name="ncc:timeInThisSmil" content="'
                + file_time_hhmmss + "." + file_time_milli_micro
                + '" />' + '\r\n')
            f_out_file.write(
                    '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
            f_out_file.write(
                '<meta name="dc:identifier" content="'
                + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
            f_out_file.write(
                '<meta name="dc:title" content="'
                + c_title + '"/>' + '\r\n')
            f_out_file.write('<layout>' + '\r\n')
            f_out_file.write('<region id="txt-view"/>' + '\r\n')
            f_out_file.write('</layout>' + '\r\n')
            f_out_file.write('</head>' + '\r\n')
            f_out_file.write('<body>' + '\r\n')
            l_file_time_seconds = str(l_file_time[n_counter - 1]).split(".")
            f_out_file.write(
                '<seq dur="' + l_file_time_seconds[0] + '.'
                + file_time_milli_micro + 's">' + '\r\n')
            f_out_file.write('<par endsync="last">' + '\r\n')
            f_out_file.write(
                '<text src="ncc.html#cnt_' + str(n_counter).zfill(4)
                + '" id="txt_' + str(n_counter).zfill(4) + '" />' + '\r\n')
            f_out_file.write('<seq>' + '\r\n')
            if file_time < timedelta(seconds=45):
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=0.000s" clip-end="npt='
                    + l_file_time_seconds[0] + '.' + file_time_milli_micro
                    + 's" id="a_' + str(n_counter).zfill(4) + '" />' + '\r\n')
            else:
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=0.000s" clip-end="npt='
                    + str(15) + '.' + file_time_milli_micro + 's" id="a_'
                    + str(n_counter).zfill(4) + '" />' + '\r\n')
                zz = n_counter + 1
                phrase_seconds = 15
                while phrase_seconds <= l_file_time[n_counter - 1] - 15:
                    f_out_file.write(
                        '<audio src="' + item + '" clip-begin="npt='
                        + str(phrase_seconds) + '.' + file_time_milli_micro
                        + 's" clip-end="npt=' + str(phrase_seconds + 15)
                        + '.' + file_time_milli_micro + 's" id="a_'
                        + str(zz).zfill(4) + '" />' + '\r\n')
                    phrase_seconds += 15
                    zz += 1
                f_out_file.write(
                    '<audio src="' + item + '" clip-begin="npt='
                    + str(phrase_seconds) + '.' + file_time_milli_micro
                    + 's" clip-end="npt=' + l_file_time_seconds[0] + '.'
                    + file_time_milli_micro + 's" id="a_' + str(zz).zfill(4)
                    + '" />' + '\r\n')

            f_out_file.write('</seq>' + '\r\n')
            f_out_file.write('</par>' + '\r\n')
            f_out_file.write('</seq>' + '\r\n')

            f_out_file.write('</body>' + '\r\n')
            f_out_file.write('</smil>' + '\r\n')
            f_out_file.close

    def split_filename(self, item):
        """split filename into list"""
        if self.comboBoxSplitterAuthorTitle.currentText() == "_-_":
            item_split = (
                item.split(self.comboBoxSplitterAuthorTitle.currentText()))

        self.show_debug_message("split filename into list")
        self.show_debug_message(item_split)
        self.show_debug_message(len(item_split))
        return item_split

    def extract_author(self, item_split):
        """extract author """
        return re.sub("_", " ", item_split[0])[2:]

    def extract_title(self, item_split):
        """extract title """
        # letzter teil
        item_left = item_split[len(item_split) - 1]
        # davon file-ext abtrennen
        item_title = item_left.split(".mp3")
        c_title = re.sub("_", " ", item_title[0])
        self.show_debug_message("extract title")
        self.show_debug_message(c_title)
        return c_title

    def action_validate_daisy(self):
        """validate daisy"""
        self.textEditValidate.append("<b>Daisy-Validierung...</b><br>")
        self.show_debug_message("validate daisy")
        # characterset of commands is importand
        # encoding in the right manner

        ncc_file = os.path.join(
            str(self.lineEditDaisySource.text()), "ncc.html")
        daisy_validator_out_path = os.path.join(
            self.app_mag_path, self.app_validator_path)

        try:
            command = (
                self.app_validator + " "
                + self.app_validator_script
                + " --ncc "
                + ncc_file
                + " --output "
                + daisy_validator_out_path
                + " --timeToleranceMs "
                + self.app_validator_time)
            self.textEditConsole.clear()
            self.pushButtonValidate.setStyleSheet(
                "background-color: red")
            self.commandLinkButton.setEnabled(False)
            self.commandLinkButtonDaisy.setEnabled(False)
            self.pushButtonValidate.setEnabled(False)
            self.process_validator.finished.connect(
                self.process_validator_finished)
            self.process_validator.start(command)
            self.movie.start()

        except Exception as e:
            errMessage = "Fehler beim Validieren: %s" % str(e)
            self.show_debug_message(errMessage)
            self.show_dialog_critical(errMessage)
            self.textEditValidate.append(
                "<font color='red'>Fehler beim Validieren</font>")

    def validate_daisy_load_report(self):
        """validate daisy load report"""

        self.textEditValidate.append("<b>Report</b>:")

        daisy_validator_out_path = os.path.join(
            self.app_mag_path, self.app_validator_path)
        daisy_validator_out_file = (
            os.path.join(
                daisy_validator_out_path,
                "html-report/html-report.html"))

        try:
            html = open(daisy_validator_out_file).read()
            self.textEditValidate.append(html2text.html2text(html))

        except Exception as e:
            errMessage = "Fehler beim Laden des Reports: %s" % str(e)
            self.show_debug_message(errMessage)
            self.show_dialog_critical(errMessage)
            self.textEditValidate.append(
                "<font color='red'>Fehler beim Laden des Reports</font>")

    def action_tab_select(self, n):
        """set focus on tab n"""
        self.tabWidget.setCurrentIndex(n)

    def show_dialog_critical(self, error_message):
        QtWidgets.QMessageBox.critical(self, "Achtung", error_message)

    def show_debug_message(self, debug_message):
        if self.app_debug_mod == "yes":
            print(debug_message)

    def action_quit(self):
        QtWidgets.qApp.quit()

    def main(self):
        self.show_debug_message("let's rock")
        config_file_ok = self.read_config()
        self.progressBarCopy.setValue(0)
        self.progressBarDaisy.setValue(0)
        # Bhz in Combo
        for item in self.app_mag_items:
            self.comboBoxCopyBhz.addItem(item)
        # Issue nr in Comboncc:maxPageNormal
        year_prev = str(datetime.datetime.now().year - 1)
        year_curr = str(datetime.datetime.now().year)
        year_next = str(datetime.datetime.now().year + 1)
        for item in self.app_issue_items_prev:
            self.comboBoxCopyBhzAusg.addItem(year_prev + "_" + item)
        for item in self.app_issue_items_current:
            self.comboBoxCopyBhzAusg.addItem(year_curr + "_" + item)
        for item in self.app_issue_items_next:
            self.comboBoxCopyBhzAusg.addItem(year_next + "_" + item)
        # Separator in Combo
        self.comboBoxSplitterAuthorTitle.addItem("_-_")
        self.comboBoxSplitterAuthorTitle.addItem("Ausgabe-Nr.")
        self.comboBoxSplitterAuthorTitle.addItem(year_prev)
        self.comboBoxSplitterAuthorTitle.addItem(year_curr)
        self.comboBoxSplitterAuthorTitle.addItem(year_next)
        self.comboBoxSplitterAuthorTitle.addItem("-")
        self.comboBoxSplitterAuthorTitle.addItem("_")

        self.comboBoxPrefBitrate.addItem("64")
        self.comboBoxPrefBitrate.addItem("96")
        self.comboBoxPrefBitrate.addItem("128")
        self.comboBoxPrefBitrate.setCurrentIndex(1)
        # Conditions Checkboxes
        self.checkBoxCopyBhzIntro.setChecked(True)
        self.checkBoxCopyBhzIssueAnnouncement.setChecked(True)
        self.checkBoxCopyID3Change.setChecked(True)
        self.checkBoxCopyBitrateChange.setChecked(True)
        # Conditions spinboxes
        self.spinBoxLevel.setValue(1)
        self.spinBoxPages.setValue(0)
        # Help-Text
        self.read_help()
        self.show()

        if config_file_ok is False:
            # don't waste time with further checks
            return
        self.check_packages(self.app_lame)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    dyc = DaisyCopy()
    dyc.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.
